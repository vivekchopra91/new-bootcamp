# Databases

## Configuring MySQL on Ubuntu

## Step 1 — Installing MySQL

Install the `mysql-server` package:

```bash
sudo apt install mysql-server
```

Ensure that the server is running using the `systemctl start` command:

```bash
sudo systemctl start mysql.service
```

These commands will install and start MySQL, but will not prompt you to set a password or make any other configuration changes.

## **Step 2 — Configuring MySQL**

Prior to July 2022, this script would silently fail after attempting to set the **root** account password and continue on with the rest of the prompts.

```bash
Output
 ... Failed! Error: SET PASSWORD has no significance for user 'root'@'localhost' as the authentication method used doesn't store authentication data in the MySQL server. Please consider using ALTER USER instead if you want to change authentication parameters.

New password:
```

This will lead the script into a recursive loop which you can only get out of by closing your terminal window.

To avoid entering this recursive loop, though, you’ll need to first adjust how your **root** MySQL user authenticates.

First, open up the MySQL prompt:

```bash
sudo mysql
```

Then run the following `ALTER USER` command to change the **root** user’s authentication method to one that uses a password.

```sql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'your_password';
```

After making this change, exit the MySQL prompt:

Run the security script with `sudo`:

```bash
sudo mysql_secure_installation
```

This will take you through a series of prompts where you can make some changes to your MySQL installation’s security options.

1. At the next prompt, the utility asks for a password for the root account. Provide and continue.
2. The following questions ask whether to remove anonymous users, to allow the `root` account to connect remotely, and to remove the `test` database. Enter `y` or `n` at each prompt according to your preferences. The `test` database is useful during initial validation, but for security reasons, it is best to disallow the `root` account from logging in remotely.

   ```bash
   By default, a MySQL installation has an anonymous user,
   allowing anyone to log into MySQL without having to have
   a user account created for them. This is intended only for
   testing, and to make the installation go a bit smoother.
   You should remove them before moving into a production
   environment.

   Remove anonymous users? (Press y|Y for Yes, any other key for No) :

   Normally, root should only be allowed to connect from
   'localhost'. This ensures that someone cannot guess at
   the root password from the network.

   Disallow root login remotely? (Press y|Y for Yes, any other key for No) :

   By default, MySQL comes with a database named 'test' that
   anyone can access. This is also intended only for testing,
   and should be removed before moving into a production
   environment.

   Remove test database and access to it? (Press y|Y for Yes, any other key for No
   ```

3. When prompted, reload the `privilege` tables to update the database.

   ```bash
   Reloading the privilege tables will ensure that all changes
   made so far will take effect immediately.

   Reload privilege tables now? (Press y|Y for Yes, any other key for No)
   ```

4. Also, allow the MySQL traffic allowed through firewall.

   ```bash
   sudo ufw status
   sudo ufw allow mysql
   ```

## Step 3 — Creating a Dedicated MySQL User and Granting Privileges

Upon installation, MySQL creates a **root** user account which you can use to manage your database. This user has full privileges over the MySQL server, meaning it has complete control over every database, table, user, and so on. Because of this, it’s best to avoid using this account outside of administrative functions. This step outlines how to use the **root** MySQL user to create a new user account and grant it privileges.

Login into MySQL with provided auth:

```bash
mysql -u root -p
```

Once you have access to the MySQL prompt, you can create a new user with a `CREATE USER` statement. These follow this general syntax:

```sql
CREATE USER 'username'@'host' IDENTIFIED WITH authentication_plugin BY 'password';
```

After `CREATE USER`, you specify a username. This is immediately followed by an `@` sign and then the hostname from which this user will connect. If you only plan to access this user locally from your Ubuntu server, you can specify `localhost`. Wrapping both the username and host in single quotes isn’t always necessary, but doing so can help to prevent errors.

```sql
CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';
```

**Note**: There is a known issue with some versions of PHP that causes problems with `caching_sha2_password`. If you plan to use this database with a PHP application — phpMyAdmin, for example — you may want to create a user that will authenticate with the older, though still secure, `mysql_native_password` plugin instead:

```sql
CREATE USER 'username'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
```

The `PRIVILEGE` value in this example syntax defines what actions the user is allowed to perform on the specified `database` and `table`. You can grant multiple privileges to the same user in one command by separating each with a comma. You can also grant a user privileges globally by entering asterisks (`*`) in place of the database and table names. In SQL, asterisks are special characters used to represent “all” databases or tables.

To illustrate, the following command grants a user global privileges to `CREATE`, `ALTER`, and `DROP` databases, tables, and users, as well as the power to `INSERT`, `UPDATE`, and `DELETE` data from any table on the server. It also grants the user the ability to query data with `SELECT`, create foreign keys with the `REFERENCES` keyword, and perform `FLUSH` operations with the `RELOAD` privilege. However, you should only grant users the permissions they need, so feel free to adjust your own user’s privileges as necessary.

You can find the full list of available privileges in [the official MySQL documentation](https://dev.mysql.com/doc/refman/8.0/en/privileges-provided.html#privileges-provided-summary).

Run this `GRANT` statement, replacing `username` with your own MySQL user’s name, to grant these privileges to your user:

```sql
GRANT CREATE, ALTER, DROP, INSERT, UPDATE, DELETE, SELECT, REFERENCES, RELOAD on *.* TO 'username'@'localhost' WITH GRANT OPTION;
```

Note that this statement also includes `WITH GRANT OPTION`. This will allow your MySQL user to grant any permissions that it has to other users on the system.

**Warning**: Some users may want to grant their MySQL user the `ALL PRIVILEGES` privilege, which will provide them with broad superuser privileges akin to the **root** user’s privileges, like so:

```sql
GRANT ALL PRIVILEGES ON *.* TO 'username'@'localhost' WITH GRANT OPTION;
```

Such broad privileges **should not be granted lightly**, as anyone with access to this MySQL user will have complete control over every database on the server.

Following this, it’s good practice to run the `FLUSH PRIVILEGES` command. This will free up any memory that the server cached as a result of the preceding `CREATE USER` and `GRANT` statements:

```
FLUSH PRIVILEGES;
```

Then you can exit the MySQL client:

## **Create a Database**

1. To create a database, log in to MySQL using an account holding `CREATE` privileges. Replace `mysqlusername` with the username you created.

   ```
   mysql -u mysqlusername -p

   ```

2. Create a new database using the `CREATE DATABASE` command. Replace `newdatabasename` with the desired name for your database.

   ```sql
   CREATE DATABASE newdatabasename;
   ```

3. To confirm the new database has been created correctly, use `SHOW DATABASES`.

   ```sql
   SHOW DATABASES;
   ```

   ```sql
   +--------------------+
   | Database           |
   +--------------------+
   ...
   | newdatabasename   |
   ...
   +--------------------+
   5 rows in set (0.00 sec)
   ```

4. Indicate the database that you want to work with using the `USE` command. Replace `newdatabasename` with the name for the database that you just created.

   ```sql
   USE newdatabasename;
   ```

   > Note
   >
   > We can also use the `USE` command when you have more than one database and you want to switch between them.

5. To find out the name of the current database, use the `SELECT DATABASE` command. The output displays the database name.

   ```sql
   SELECT DATABASE();
   ```

   ```sql
   +------------------+
   | DATABASE()       |
   +------------------+
   | newdatabasename |
   +------------------+
   ```

### **Create a Table**

At this point, the database, `newdatabasename` does not have any tables, so it is not possible to store any data in it yet. To define a table, use the `CREATE TABLE` command. Along with the name of the table, this command requires the name and data type of each field. The data type characterizes the data stored in the field. For example, the data type could be a variable-length string, known as a `VARCHAR`. For a complete list of data types, consult the **[MySQL documentation](https://dev.mysql.com/doc/refman/8.0/en/data-types.html)**. Some of the more common data types are as follows.

- **INT:** This can contain a value between `2147483648` and `2147483647`. If specified as `UNSIGNED`, it can store values between `0` and `4294967295`.
- **SMALLINT:** Holds an even smaller integer value between `32768` and `32767`.
- **FLOAT:** This type can store a floating-point number.
- **DATE:** Stores a date in `YYYY-MM-DD` format.
- **DATETIME:** Stores a date and time combination in `YYYY-MM-DD HH:MM:SS` format. The same time can be stored without dashes and colons in the `TIMESTAMP` format.
- **VARCHAR(N):** This is a variable-length string between `1` and `N` characters in length, with a maximum length of `255` characters.
- **TEXT:** This data type holds up to `65535` characters. It can hold text, images, or binary data.
- **CHAR(N):** This type represents a fixed-length text field of length `N`. For example, to hold two-character state codes, use a data type of `CHAR(2)`.

To create a table, follow these steps:

1. While logged in to MySQL, switch to the database where you want to add the table.

   ```sql
   use newdatabasename;
   ```

2. Use the `CREATE TABLE` command to generate a new table. Use the format `CREATE TABLE table_name (field_1 datatype, field_n datatype);`.

   ```sql
   CREATE TABLE newtablename (column1 VARCHAR(20), column2 CHAR(1), column3 DATE, column4 SMALLINT UNSIGNED);
   ```

3. To confirm the table now exists, use the `SHOW TABLES` command.

   ```sql
   SHOW TABLES;
   ```

   ```sql
   +----------------------------+
   | Tables_in_newdatabasename |
   +----------------------------+
   | newtablename              |
   +----------------------------+
   1 row in set (0.00 sec)
   ```

4. To review the table structure and verify the list of fields, use the `DESCRIBE` command.

   ```sql
   DESCRIBE newtablename;
   ```

   ```sql
   +---------+-------------------+------+-----+---------+-------+
   | Field   | Type              | Null | Key | Default | Extra |
   +---------+-------------------+------+-----+---------+-------+
   | column1 | varchar(20)       | YES  |     | NULL    |       |
   | column2 | char(1)           | YES  |     | NULL    |       |
   | column3 | date              | YES  |     | NULL    |       |
   | column4 | smallint unsigned | YES  |     | NULL    |       |
   +---------+-------------------+------+-----+---------+-------+
   4 rows in set (0.00 sec)
   ```

5. If a table is no longer required, delete it using the `DROP TABLE` command.

   > Caution
   >
   > When a table is dropped, all data inside the table is lost and cannot be recovered.

   ```
   DROP TABLE newtablename;
   ```

### **Add and Retrieve Data**

The main way to insert a new row of data into a table is with the `INSERT` command.

1. To add a row, use the `INSERT` command. Specify the table name, the keyword `VALUES`, and a bracketed, comma-separated list of values in the format `INSERT INTO tablename VALUES ('value_1', ... 'value_n');`. The column values must have the same sequence as the table definition, with the string and date values in quotes. For example, to add data to `newtablename`, specify values for `column1`, `column2`, `column3`, and `column4`, in that order.

   ```sql
   INSERT INTO newtablename VALUES ('value1','a','2021-09-10',123);
   ```

2. To retrieve data, use the `SELECT` command, along with some constraints telling MySQL which rows to return. The entire contents of the table can be returned, or only a subset. To select all rows in a table, use the `SELECT *` command, but do not add any qualifiers.

   ```sql
   SELECT * FROM newtablename;
   ```

   ```sql
   +---------+---------+------------+---------+
   | column1 | column2 | column3    | column4 |
   +---------+---------+------------+---------+
   | value1  | a       | 2021-09-10 |     123 |
   | value2  | b       | 2021-09-08 |     123 |
   +---------+---------+------------+---------+
   2 rows in set (0.00 sec)
   ```

3. It is also possible to only select rows fitting particular criteria, for example, where a column is set to a certain value. Use the `WHERE` keyword as a qualifier, followed by the match criteria as a constraint. In this example, only rows in which `column2` is set to `a` are displayed.

   ```sql
   SELECT * FROM newtablename WHERE column2 = 'a';
   ```

   ```sql
   +---------+---------+------------+---------+
   | column1 | column2 | column3    | column4 |
   +---------+---------+------------+---------+
   | value1  | a       | 2021-09-08 |     123 |
   +---------+---------+------------+---------+
   1 row in set (0.00 sec)
   ```

4. For tables with many columns, it is often easier to limit the information that is displayed. To only select certain columns for each row, specify the column names instead of the `` symbol.

   ```sql
   SELECT column1, column4 FROM newtablename;
   ```

   ```sql
   +---------+---------+
   | column1 | column4 |
   +---------+---------+
   | value1  |     123 |
   +---------+---------+
   2 rows in set (0.00 sec)
   ```

5. To modify a row in a table, use the `UPDATE` command. The `SET` keyword indicates the column to update and the new value. If necessary, the `WHERE` keyword provides a method of constraining the operation to only apply to certain rows. In the following example, the value of `column4` is only changed to `155` if `column2` is equal to `b`.

   ```sql
   UPDATE newtablename SET column4 = 155 WHERE column2 = 'a';
   ```

6. The `SELECT *` statement can be used to confirm the update.

   ```sql
   SELECT * FROM newtablename;
   ```

   ```sql
   +---------+---------+------------+---------+
   | column1 | column2 | column3    | column4 |
   +---------+---------+------------+---------+
   | value1  | a       | 2021-09-10 |     123 |
   +---------+---------+------------+---------+
   2 rows in set (0.00 sec)
   ```

## Backup DB

## **Create a Backup (Physical)**

1. Locate your database directory. It should be `/var/lib/mysql/` on most systems but if that directory doesn’t exist, examine `/etc/mysql/my.cnf` for a path to the data directory.
2. Create a directory to store your backups. This guide will use `/opt/db-backups` but you can alter this to suit your needs:

   ```bash
   mkdir /opt/db-backups
   ```

3. Copy MySQL’s data directory to a storage location. The `cp` command, `rsync`, or other methods will work fine, but we’ll use `tar` to recursively copy and gzip the backup at one time. Change the database directory, backup filename, and target directory as needed; the `$(date +%F)` addition to the command will insert a timestamp into the filename.

   ```bash
   tar cfvz /opt/db-backups/db-$(date +%F).tar.gz /var/lib/mysql/*
   ```

4. Restart the MySQL service:

   ```bash
   systemctl restart mysql
   ```

## **Restore a Backup**

1. Change your working directory to a place where you can extract the tarball created above. The current user’s home directory is used in this example:

   ```bash
   cd
   ```

2. Stop the `mysql` service:

   ```bash
   systemctl stop mysql
   ```

3. Extract the tarball to the working directory. Change the tarball’s filename in the command to the one with the date you want to restore to.

   ```bash
    tar zxvf /opt/db-backups/db-archive.tar.gz -C .
   ```

4. Move the current contents of `/var/lib/mysql` to another location if you want to keep them for any reason, or delete them entirely. Create a new empty `mysql` folder to restore your backed up DMBS into.

   ```bash
   mv /var/lib/mysql /var/lib/mysql-old
   mkdir /var/lib/mysql
   ```

5. Copy the backed up database system to the empty folder:

   ```bash
   mv ~/var/lib/mysql/* /var/lib/mysql
   ```

6. Set the proper permissions for the files you just restored:

   ```bash
   chown -R mysql:mysql /var/lib/mysql
   ```

7. Restart the MySQL service:

   ```bash
   systemctl restart mysql
   ```

## Using mysqldump

## **General mysqldump Syntax**

The following list represents mysqldump commands for various scenarios. Within the commands, `[options]` represents all of the command options required to perform the backup according to your own needs.

- **Single database**:
  ```bash
  mysqldump [options] [database_name] > backup.sql
  ```
- **Specific tables in single database**:
  ```bash
  mysqldump [options] [database_name] [table_name] > backup.sql
  ```
- **Multiple specific databases**:
  ```bash
  mysqldump [options] --databases [database1_name] [database2_name] > backup.sql
  ```
- **All databases**:
  ```bash
  mysqldump [options] --all-databases > backup.sql
  ```

### [Common options](https://www.linode.com/docs/guides/mysqldump-backups/#common-command-options)

- **Username** (`-user=[]` or `u []`): The username of your MySQL user. This user must have proper grants to access the database.
- **Password** (`-password=[]` or `p[]`): Specifies that the user’s password is required for the connection. The password can be entered directly in the command itself (though that is not recommended due to security concerns) or the password can be omitted (by just using the `-password` option with no value). In the password is omitted, mysqldump prompts you for the password before connecting to the database.
- **Host** (`-host=[]` or `h []`): The IP address or hostname of the remote database server. You can omit this option from the command if you are connecting to a local MySQL instance on your same system.
- **Port** (`-port=[]` or `P []`): The port number of that the MySQL database instance uses. This can be omitted if your MySQL instance uses the default port of `3306`.
- **Output file** (`> backup.sql`): The name of the output file. To keep your backups organized with unique filenames, it may be helpful to add an automatically generated timestamp (ex: `backup-$(date +%F).sql` for just the date or `backup-$(date +%Y%m%d-%H%M%S).sql` for the date and time).
