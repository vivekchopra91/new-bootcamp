### What is an SSL certificate?

An SSL certificate is a digital certificate that authenticates a website's identity and enables an encrypted connection. SSL stands for Secure Sockets Layer, a security protocol that creates an encrypted link between a web server and a web browser.

In short: SSL keeps internet connections secure and prevents criminals from reading or modifying information transferred between two systems. When you see a padlock icon next to the URL in the address bar, that means SSL protects the website you are visiting

## How do SSL certificates work?

SSL works by ensuring that any data transferred between users and websites, or between two systems, remains impossible to read. It uses encryption algorithms to scramble data in transit, which prevents hackers from reading it as it is sent over the connection. This data includes potentially sensitive information such as names, addresses, credit card numbers, or other financial details.

The process works like this:

1. A browser or server attempts to connect to a website (i.e., a web server) secured with SSL.
2. The browser or server requests that the web server identifies itself.
3. The web server sends the browser or server a copy of its SSL certificate in response.
4. The browser or server checks to see whether it trusts the SSL certificate. If it does, it signals this to the webserver.
5. The web server then returns a digitally signed acknowledgment to start an SSL encrypted session.
6. Encrypted data is shared between the browser or server and the webserver.

## Why you need an SSL certificate

Websites need SSL certificates to keep user data secure, verify ownership of the website, prevent attackers from creating a fake version of the site, and convey trust to users.

An SSL certificate helps to secure information such as:

- Login credentials
- Credit card transactions or bank account information
- Personally identifiable information — such as full name, address, date of birth, or telephone number
- Legal documents and contracts
- Medical records
- Proprietary information

## Types of SSL certificate

There are different types of SSL certificates with different validation levels. The six main types are:

1. Extended Validation certificates (EV SSL)
2. Organization Validated certificates (OV SSL)
3. Domain Validated certificates (DV SSL)
4. Wildcard SSL certificates
5. Multi-Domain SSL certificates (MDC)
6. Unified Communications Certificates (UCC)

## Self-Signed SSL Certificate

## Step 1 — Enabling `mod_ssl`

Before we can use *any* SSL certificates, we first have to enable `mod_ssl`, an Apache module that provides support for SSL encryption.

Enable `mod_ssl` with the `a2enmod` command:

```bash
sudo a2enmod ssl
```

Restart Apache to activate the module:

```bash
sudo systemctl restart apache2

```

The `mod_ssl` module is now enabled and ready for use.

## Step 2 — Create the SSL Certificate

Now that Apache is ready to use encryption, we can move on to generating a new SSL certificate. The certificate will store some basic information about your site, and will be accompanied by a key file that allows the server to securely handle encrypted data.

We can create the SSL key and certificate files with the `openssl` command:

```bash
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt
```

After you enter the command, you will be taken to a prompt where you can enter information about your website. Before we go over that, let’s take a look at what is happening in the command we are issuing:

- openssl: This is the basic command line tool for creating and managing OpenSSL certificates, keys, and other files.
- req: This subcommand specifies that we want to use X.509 certificate signing request (CSR) management. The “X.509” is a public key infrastructure standard that SSL and TLS adheres to for its key and certificate management. We want to create a new X.509 cert, so we are using this subcommand.
- x509: This further modifies the previous subcommand by telling the utility that we want to make a self-signed certificate instead of generating a certificate signing request, as would normally happen.
- nodes: This tells OpenSSL to skip the option to secure our certificate with a passphrase. We need Apache to be able to read the file, without user intervention, when the server starts up. A passphrase would prevent this from happening because we would have to enter it after every restart.
- days 365: This option sets the length of time that the certificate will be considered valid. We set it for one year here.
- newkey rsa:2048: This specifies that we want to generate a new certificate and a new key at the same time. We did not create the key that is required to sign the certificate in a previous step, so we need to create it along with the certificate. The `rsa:2048` portion tells it to make an RSA key that is 2048 bits long.
- keyout: This line tells OpenSSL where to place the generated private key file that we are creating.
- out: This tells OpenSSL where to place the certificate that we are creating.

As we stated above, these options will create both a key file and a certificate. We will be asked a few questions about our server in order to embed the information correctly in the certificate.

Fill out the prompts appropriately. The most important line is the one that requests the `Common Name`. You need to enter either the hostname you’ll use to access the server by, or the public IP of the server. It’s important that this field matches whatever you’ll put into your browser’s address bar to access the site, as a mismatch will cause more security errors.

The entirety of the prompts will look something like this:

```bash
Country Name (2 letter code) [XX]:US
State or Province Name (full name) []:Example
Locality Name (eg, city) [Default City]:Example
Organization Name (eg, company) [Default Company Ltd]:Example Inc
Organizational Unit Name (eg, section) []:Example Dept
Common Name (eg, your name or your server's hostname) []:your_domain_or_ip
Email Address []:webmaster@example.com
```

Both of the files you created will be placed in the appropriate subdirectories of the `/etc/ssl` directory.

## Step 3 — Configure Apache to Use SSL

```bash
sudo nano /etc/apache2/sites-available/your_domain_or_ip.conf

```

```bash
<VirtualHost *:443>
   ServerNameyour_domain_or_ip
   DocumentRoot /var/www/your_domain_or_ip

   SSLEngine on
   SSLCertificateFile /etc/ssl/certs/apache-selfsigned.crt
   SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key
</VirtualHost>
```

```bash
sudo mkdir /var/www/your_domain_or_ip
```

Open a new `index.html` file with your text editor:

```bash
sudo nano /var/www/your_domain_or_ip/index.html
```

Save and close the file Next, we need to enable the configuration file with the `a2ensite` tool:

```bash
sudo a2ensiteyour_domain_or_ip.conf
```

It will prompt you to restart Apache to activate the configuration, but first, let’s test for configuration errors:

```bash
sudo apache2ctl configtest

```

If everything is successful, you will get a result that looks like this:

```bash
Output
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1. Set the 'ServerName' directive globally to suppress this message
Syntax OK
```

If your output has `Syntax OK` in it, your configuration file has no syntax errors. We can safely reload Apache to implement our changes:

```bash
sudo systemctl reload apache2
```

Now load your site in a browser, being sure to use `https://` at the beginning.
