#!/bin/bash

# Name Of the Script : mysql-service
# AUTHOR : Akshay Khandelwal
# DATE : 12th July 2022
# PLATFORM : All Unix/Linux
# VERSION : 1.0.0 

# Purpose : Install and configure MySQL on Amazon Linux
 
# Install an RPM repository package by running the commands below:
sudo yum install https://dev.mysql.com/get/mysql80-community-release-el7-5.noarch.rpm -y 

# Config repository
sudo yum repolist

# Install MYSQL-8 
sudo amazon-linux-extras install epel -y
sudo yum -y install mysql-community-server

# Start and Configure MySQL on Amazon Linux 
sudo systemctl enable mysqld

sudo systemctl status mysqld

sudo systemctl restart mysqld

# Create file for Monitoring the mysqld service
cp -pvr monitor-mysql-service.sh /opt/

# Get the temp login password
sudo grep 'temporary password' /var/log/mysqld.log 

# End Of Script 
exit 0 