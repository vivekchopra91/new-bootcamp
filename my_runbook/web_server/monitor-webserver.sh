#!/bin/bash

# Create a Variable 
STATUS="$(systemctl is-active mysqld.service)"
SERVICE_NAME="nginx"

# Conditional Statement 
if [ "$STATUS" == "inactive" ];
then
    echo "Starting the $SERVICE_NAME Web Service..."
    systemctl restart $SERVICE_NAME.service
else
    echo "`date +%Y-%m-%d` $SERVICE_NAME Service is Healthy and Running & PID = `ps -aux | grep $SERVICE_NAME | grep -v grep | grep mysql | awk -F " " '{print $2}'`"
fi

# Creating the log file for mysql

touch /opt/mysqld.log

exit 0