#!/bin/env bash

# Name Of the Script : web-apache
# AUTHOR : Akshay Khandelwal
# DATE : 12th July 2022
# PLATFORM : All Unix/Linux
# VERSION : 1.0.0 

# Purpose : Install and configure apache2 on Ubuntu

# Update package
apt update 

# Variables
hname="apache.akshay"

# Hostname Change
hostnamectl set-hostname $hname 
echo "`hostname -I | awk '{ print $1 }'` `hostname`" >> /etc/hosts 

# Download, & Install Utility Softwares 
apt install git wget unzip curl tree net-tools -y 

# Download, Install apache2 Web Server 
apt install apache2 -y 

# Check the status
systemctl enable apache2
systemctl start apache2
systemctl status apache2

# Create index.html file
echo "Creating HTML dir:"
read file_name
mkdir /var/www/$file_name
cp -pvr index.html /var/www/$file_name/

# End Of Script 
exit 0

