#!/bin/env bash

# Name Of the Script : web-nginx
# AUTHOR : Akshay Khandelwal
# DATE : 12th July 2022
# PLATFORM : All Unix/Linux
# VERSION : 1.0.0 

# Purpose : Install and configure Nginx on Ubuntu

# Update package
sudo apt update 

# Variables
hname="nginx.akshay"

# Logic
sudo hostnamectl set-hostname $hname 
echo "`hostname -I | awk '{ print $1 }'` `hostname`" >> /etc/hosts 

# Download, & Install Utility Softwares 
sudo apt install git wget unzip curl tree net-tools -y 

# Download, Install Nginx Web Server 
sudo apt install nginx -y 

# Check the status
sudo systemctl enable nginx
sudo systemctl start nginx
sudo systemctl status nginx

# Create index.html file
echo "Creating HTML dir:"
read file_name
mkdir /var/www/$file_name
cp -pvr index.html /var/www/$file_name/

# Setting up the Virtual host 
echo "server {
       listen 81;
       listen [::]:81;

       server_name web.$file_name.com;

       root /var/www/$file_name;
       index index.html;

       location / {
               try_files $uri $uri/ =404;
       }
}" >> /etc/nginx/sites-available/$file_name

ln -s /etc/nginx/sites-available/$file_name /etc/nginx/sites-enabled/

# Restart the service 
sudo systemctl restart nginx


