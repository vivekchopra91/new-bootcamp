#!/bin/bash

# Name Of the Script : web-httpd
# AUTHOR : Akshay Khandelwal
# DATE : 12th July 2022
# PLATFORM : All Unix/Linux
# VERSION : 1.0.0 

# Purpose : Install and configure Apache on Amazon Linux

# Update package
sudo yum update -y

# Variables
hname="apache.akshay"

# Logic
sudo hostnamectl set-hostname $hname 
echo "`hostname -I | awk '{ print $1 }'` `hostname`" >> /etc/hosts 

# Download, & Install Utility Softwares 
sudo yum install git wget unzip curl tree firewalld -y 

# Download, Install & Configure Web Server i.e. RHEL/CENTOS/AmazonLinux/Oracle Linux - httpd 
sudo yum install httpd -y 

# Check the status
sudo systemctl enable httpd
sudo systemctl restart httpd
sudo systemctl status httpd

# Deploy a simple Website Part of DocumentRoot
echo "Create a log directory":
read filename
mkdir -p /var/www/html/$filename.com    # DocumentRoot
mkdir -p /var/log/httpd/$filename.com   # Log Directory
cp -pvr index.html /var/www/html/

# firewall-cmd --zone=public --permanent --add-service=http
# firewall-cmd --reload

# End Of Script 
exit 0

