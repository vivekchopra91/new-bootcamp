**Step 1 — Installing Apache and Updating the Firewall**

Then, install Apache with:

```bash
sudo apt-get update
sudo apt-get install apache2 apache2-utils
```

Once the installation is finished, you’ll need to adjust your firewall settings to allow HTTP traffic. UFW has different application profiles that you can leverage for accomplishing that. To list all currently available UFW application profiles, you can run:

```bash
sudo ufw app list
```

```bash
Output
Available applications:
ApacheApache FullApache Secure
  OpenSSH
```

Here’s what each of these profiles mean:

- **Apache**: This profile opens only port `80` (normal, unencrypted web traffic).
- **Apache Full**: This profile opens both port 80 (normal, unencrypted web traffic) and port 443 (TLS/SSL encrypted traffic).
- **Apache Secure**: This profile opens only port `443` (TLS/SSL encrypted traffic).

To only allow traffic on port `80`, use the `Apache` profile:

```bash
sudo ufw allow in "Apache"
```

Traffic on port `80` is now allowed through the firewall.

## **Configure Apache Virtual Hosts**

1. Create a virtual hosts configuration file for `example1.com` and add the example virtual host block into `/etc/apache2/sites-available/example1.com`. Be sure to replace all instances of `example1.com` with your own domain.

   File: /etc/apache2/sites-available/
   ``

   ```bash
   <VirtualHost *:80>
     # The primary domain for this host
     ServerName example2.com
     # Optionally have other subdomains also managed by this Virtual Host
     ServerAlias example2.com *.example2.com
     DocumentRoot /var/www/html/example2.com/public_html
     <Directory /var/www/html/example2.com/public_html>
         Require all granted
         # Allow local .htaccess to override Apache configuration settings
         AllowOverride all
     </Directory>
   ```

**Step 2 — Installing MySQL**

[Check the installation of MySQL](../database/mysql/mysql.md)

**Step 3 — Installing PHP**

Installing PHP and few modules for it to work with web and database server.

```bash
sudo apt-get install php libapache2-mod-php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip
```

Furthermore, to test if **php** is working in collaboration with the webserver, we need to create a **`info.php`** file inside **/var/www/html**.

```bash
sudo vi /var/www/html/info.php
```

And paste the code below into the file, save it, and exit.

```bash
<?php
phpinfo();
?>
```

When that is done, open your web browser and type in the following URL in the address bar.

**Step 4 — Installing WordPress**

Then move the WordPress files from the extracted folder to the Apache default root directory, **/var/www/html/**:

Download the latest version of the WordPress package and extract it by issuing the commands below on the terminal:

```bash
wget -c http://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz
```

```bash
sudo mv wordpress/* /var/www/html/
```

Set the permissions

```bash
sudo chown -R www-data:www-data /var/www/html/
sudo chmod -R 755 /var/www/html/
```

**Step 5 — Create a WordPress DB**

Go the **/var/www/html/** directory and rename existing **`wp-config-sample.php`** to **`wp-config.php`**. Also, make sure to remove the default Apache index page.

```bash
cd /var/www/html/
sudo mv wp-config-sample.php wp-config.php
sudo rm -rf index.html
```

Then, config the `wp-config.php` file according to the database user and password.

Restart the web server and mysql service using the commands below:

```bash
sudo systemctl restart apache2.service
sudo systemctl restart mysql.service
```

Go to the site on configure the WordPress Login.
